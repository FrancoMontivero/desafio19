"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function products(mongoose) {
    const schema = new mongoose.Schema({
        id: { type: Number },
        title: { type: String, require: true },
        price: { type: Number, require: true },
        thumbnail: { type: String, require: true }
    });
    const model = mongoose.model("product", schema);
    return { name: "Product", model };
}
exports.default = products;

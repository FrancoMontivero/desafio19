export default function message(mongoose: any) {
    const schema = new mongoose.Schema({
        from: {type: String, require: true},
        message: {type: String, require: true},
        hour: {type: String, require: true},
        date: {type: String, require: true}
    })

    const model = mongoose.model("message", schema)

    return {name: "Message", model};
}
import fs from 'fs';
import path from 'path';
import mongoose from 'mongoose';

import { HOST_MONGO } from '../config'

const basename = path.basename(__filename);

const URL = `${HOST_MONGO}/ecommerce`; 

(async function () {
  await mongoose.connect(URL, {useNewUrlParser: true, useUnifiedTopology: true});
})(); 


let db: {[key: string] : any}= {};

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(async file => {
    let model = await import(path.join(__dirname, file)).then(model => model['default'](mongoose));
    db[model.name] = model.model;
  });

export default db;
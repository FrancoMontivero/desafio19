import express from 'express';

import { getProducts, getProduct, addProduct, updateProduct, removeProduct } from '../controllers/products';

const routerProducts = express.Router();

routerProducts.get('/listar', async (req, res) => {
	try { 
		const products = await getProducts();
		if(products.length === 0) res.json({error: "No hay productos cargados"});
		else res.json(products); 
	}
	catch (err) { res.json({"error": err.message}) };
})

routerProducts.get('/listar/:id', async (req, res) => {
	try { 
		const product = await getProduct(parseInt(req.params.id));
		res.json(product); 
	}
	catch (err) { res.json({"error": err.message}); };
})

routerProducts.post('/guardar', async (req, res) => {
	const { title, price, thumbnail } = req.body;
	try { res.json(await addProduct(title, price, thumbnail)); }
	catch (err) { res.json({"error": err.message}); };
})

routerProducts.put('/actualizar/:id', async (req, res) => {
	const { title, price, thumbnail } = req.body;
	const id: number = parseInt(req.params.id);
	try { res.json(await updateProduct(id, title, price, thumbnail)); }
	catch (err) { res.json({ "error": err.message }); };
})

routerProducts.delete('/borrar/:id',async (req, res) => {
	const id: number = parseInt(req.params.id);
	try { res.json(await removeProduct(id)); }
	catch (err) { res.json({"error": err.message}) }
})

export default routerProducts;